package com.bsd.xxyp.rdlock.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author ：wangcheng
 * @date 2022/12/30
 **/
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class LockInfo<T> {

    private String lockKey;

    private String lockValue;

    private Long waitTime;

    private Long lockTimeout;

    private T lockClient;

    private boolean lockSuccess;
}
