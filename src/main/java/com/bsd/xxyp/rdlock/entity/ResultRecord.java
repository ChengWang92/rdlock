package com.bsd.xxyp.rdlock.entity;

import com.alibaba.fastjson.annotation.JSONField;
import com.bsd.xxyp.rdlock.context.LockAnnotationDataExtractor;
import lombok.Data;

/**
 * @author ：wangcheng
 * @date 2023/1/3
 **/
@Data
public class ResultRecord {

    private String className;

    private String methodName;

    private String executorName;

    private String lockKey;

    private boolean lockResult;

    @JSONField(serialize = false)
    private LockAnnotationDataExtractor extractor;

    public ResultRecord(LockAnnotationDataExtractor extractor,String executorName) {
        this.extractor = extractor;
        this.className = extractor.getClassSimpleName();
        this.methodName=extractor.getMethodName();
        this.executorName=executorName;
    }
}
