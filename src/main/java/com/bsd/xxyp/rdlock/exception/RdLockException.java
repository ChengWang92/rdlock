package com.bsd.xxyp.rdlock.exception;

/**
 * @author ：wangcheng
 * @date 2022/12/13
 **/
public class RdLockException extends RuntimeException{

    private static final int FAILURE_CODE = 300;

    private int code;
    private String desc;

    public RdLockException(int code, String desc) {
        super(desc);
        this.code = code;
        this.desc = desc;
    }

    public RdLockException(String desc) {
        super(desc);
        this.code = FAILURE_CODE;
        this.desc = desc;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    @Override
    public String toString() {
        return "RdLockException{" +
                "code=" + code +
                ", desc='" + desc + '\'' +
                '}';
    }
}
