package com.bsd.xxyp.rdlock.parse;

import com.bsd.xxyp.rdlock.context.LockAnnotationDataExtractor;
import com.bsd.xxyp.rdlock.context.LockHolder;

public interface LockKeyProcessor {


    String DEFAULT_SEPARATOR = ":";

    String buildLockKey(LockAnnotationDataExtractor extractor);

}
