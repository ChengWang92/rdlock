package com.bsd.xxyp.rdlock.parse;

import com.bsd.xxyp.rdlock.context.LockAnnotationDataExtractor;
import org.springframework.context.expression.MethodBasedEvaluationContext;
import org.springframework.core.DefaultParameterNameDiscoverer;
import org.springframework.core.ParameterNameDiscoverer;

/**
 * @author ：wangcheng
 * @date 2022/12/9
 **/
public class LockEvaluationContextProvider {

    private static ParameterNameDiscoverer nameDiscoverer = new DefaultParameterNameDiscoverer();

    public MethodBasedEvaluationContext create(LockAnnotationDataExtractor extractor) {
       return new MethodBasedEvaluationContext(null, extractor.getTargetMethod(), extractor.getParameterValues(), nameDiscoverer);
    }
}
