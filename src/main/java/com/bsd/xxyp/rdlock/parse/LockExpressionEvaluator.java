package com.bsd.xxyp.rdlock.parse;

import org.springframework.context.expression.AnnotatedElementKey;
import org.springframework.context.expression.CachedExpressionEvaluator;
import org.springframework.expression.EvaluationContext;
import org.springframework.expression.Expression;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author ：wangcheng
 * @date 2022/12/9
 **/
public class LockExpressionEvaluator extends CachedExpressionEvaluator {

    private static final Map<CachedExpressionEvaluator.ExpressionKey, Expression> CACHE_KEY_MAP = new ConcurrentHashMap<>();

    public <T>T parseExpression(String el,AnnotatedElementKey annotatedElementKey,EvaluationContext evaluationContext,Class<T> resultType) {

        Expression expression = getExpression(CACHE_KEY_MAP, annotatedElementKey, el);

        return expression.getValue(evaluationContext,resultType);
    }
}
