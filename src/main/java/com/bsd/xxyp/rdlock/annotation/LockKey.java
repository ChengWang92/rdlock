package com.bsd.xxyp.rdlock.annotation;

import java.lang.annotation.*;

/**
 * @author ：wangcheng
 * @date 2022/12/9
 **/
@Target({ElementType.FIELD,ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface LockKey {

    int sort() default 999;
}
