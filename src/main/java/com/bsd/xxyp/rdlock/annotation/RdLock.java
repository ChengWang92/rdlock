package com.bsd.xxyp.rdlock.annotation;

import com.bsd.xxyp.rdlock.executor.LockExecutor;

import java.lang.annotation.*;

@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
/**
 * @author wangcheng
 * @date 2020/8/12
 */
public @interface RdLock {

    /**
     * 业务key前缀
     * @return: java.lang.String
    **/
    String bizKey() default "";

    /**
     * 动态参数 格式 单个用#xxx ,多个用{#xxx,#xxx},对象使用 #user.getName()或者#user.name
     * @return: java.lang.String
    **/
    String value() default "";

    /**
     * 是否允许动态参数value有空值，不允许默认抛异常提示
     **/
    boolean allowValueBlank() default false;

    /**
     * 锁超时释放时间,避免死锁(根据业务合理设定)
     * @return: long
    **/
    long lockTimeOut() default 30000L;

    /**
     * 是否尝试阻塞重新获取锁
     * @return: boolean
    **/
    boolean attemptedWait() default true;

    int retryCount() default 3;

    /**
     * @Description 重试等待间隔
     * @return long
    **/
    long retryInterval() default 500L;

    /**
     * 执行完是否释放锁，否则等待锁过期时间，不支持zk使用
    **/
    boolean autoRelease() default true;

    /**
     * 获取不到锁,异常提示
     * @return: java.lang.String
    **/
    String errMsg() default "请稍后再试";

    /**
     * 自定义选择执行器
    **/
    LockExecutor.Executor executor() default LockExecutor.Executor.DEFAULT;



}
