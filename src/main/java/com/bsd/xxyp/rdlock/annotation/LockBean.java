package com.bsd.xxyp.rdlock.annotation;

import java.lang.annotation.*;

/**
 * @author ：wangcheng
 * @date 2022/12/26
 **/
@Target({ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface LockBean {

}
