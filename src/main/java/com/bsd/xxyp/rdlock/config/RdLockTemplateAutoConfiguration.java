package com.bsd.xxyp.rdlock.config;

import com.bsd.xxyp.rdlock.handler.DefaultLockTemplate;
import com.bsd.xxyp.rdlock.handler.LockTemplate;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author ：wangcheng
 * @date 2023/1/3
 **/
@Configuration
public class RdLockTemplateAutoConfiguration {

    @Bean
    public LockTemplate lockTemplate() {
        DefaultLockTemplate template =  new DefaultLockTemplate();
        template.setLockTimeOut(60 * 1000L);
        return template;
    }
}
