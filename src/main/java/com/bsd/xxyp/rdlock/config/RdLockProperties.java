package com.bsd.xxyp.rdlock.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
/**
 * @author ：wangcheng
 * @date 2022/12/13
 **/
@Data
@ConfigurationProperties(prefix = "bsd.xxxp.lock")
public class RdLockProperties {

    private String prefix;

    private String executorName;

    private boolean enable;



}

