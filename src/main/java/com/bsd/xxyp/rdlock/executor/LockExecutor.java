package com.bsd.xxyp.rdlock.executor;

import com.bsd.xxyp.rdlock.context.LockAnnotationDataExtractor;
import com.bsd.xxyp.rdlock.entity.LockInfo;
import lombok.Data;
import lombok.Getter;
import org.apache.logging.log4j.util.Strings;

import java.util.UUID;
import java.util.concurrent.locks.Lock;

public interface LockExecutor<T> {

    boolean tryAcquire(LockInfo<T> lockInfo);

    boolean releaseLock(LockInfo<T> lockInfo);

    default String getRequestId() {
        return UUID.randomUUID().toString().replace("-","");
    }

    @Getter
    enum Executor {
        DEFAULT("",null),
        JEDIS("jedis", JedisLockExecutor.class),
        REDISSON("redisson",RedissonLockExecutor .class),
        ZK("zk",ZkCuratorLockExecutor .class)
        ;
        private String name;

        private Class<? extends LockExecutor> lockExecutor;

        Executor(String name, Class<? extends LockExecutor> lockExecutor) {
            this.name = name;
            this.lockExecutor = lockExecutor;
        }

        public static Class<? extends LockExecutor> defaultExecutor() {
            return JEDIS.lockExecutor;
        }

        public static Class<? extends LockExecutor> getExecutor(String name) {
            if (Strings.isBlank(name)) return null;
            for (Executor value : Executor.values()) {
                if (value.name.equals(name)) {
                    return value.lockExecutor;
                }
            }
            return null;
        }
    }

}
