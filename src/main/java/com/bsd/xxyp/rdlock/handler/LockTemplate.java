package com.bsd.xxyp.rdlock.handler;

import com.bsd.xxyp.rdlock.executor.LockExecutor;

/**
 * @author ：wangcheng
 * @date 2023/1/2
 **/
public interface LockTemplate {

    boolean lock(String key);

    boolean lock(String key, LockExecutor.Executor executor);

    boolean lock(String key,Long lockTimeOut);

    boolean lock(String key,Long lockTimeOut,LockExecutor.Executor executor);

    boolean unLock();

    boolean unLock(LockExecutor.Executor executor);
}
