package com.bsd.xxyp.rdlock.handler;

import com.bsd.xxyp.rdlock.entity.ResultRecord;

/**
 * @author ：wangcheng
 * @date 2023/1/1
 **/
public interface LockResultHandler {

    void record(ResultRecord resultRecord);
}
