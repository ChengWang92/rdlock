package com.bsd.xxyp.rdlock.context;

import com.bsd.xxyp.rdlock.annotation.RdLock;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.util.Assert;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

/**
 * @author ：wangcheng
 * @date 2022/12/7
 **/
public class LockAnnotationDataExtractor {

    private RdLock rdLock;
    private ProceedingJoinPoint pjp;
    private Class targetClass;
    private Method targetMethod;
    private Object[] parameterValues;
    private String[] parameterNames;

    public LockAnnotationDataExtractor(ProceedingJoinPoint pjp) {
        this.pjp = pjp;
        this.targetClass = pjp.getSignature().getDeclaringType();
        MethodSignature signature = (MethodSignature) pjp.getSignature();
        this.targetMethod = signature.getMethod();
        this.rdLock = AnnotationUtils.getAnnotation(targetMethod,RdLock.class);
        Assert.notNull(rdLock,"rdLock annotation is null");
        this.parameterNames = signature.getParameterNames();
        this.parameterValues = pjp.getArgs();
    }

    public Map<String,Object> getParameter() {
        Map<String,Object> param = new HashMap<>();
        for (int i = 0; i < parameterNames.length; i++) {
            param.put(parameterNames[i],parameterValues[i]);
        }
        return param;
    }


    public RdLock getRdLock() {
        return rdLock;
    }

    public Class getTargetClass() {
        return targetClass;
    }

    public Method getTargetMethod() {
        return targetMethod;
    }

    public Object[] getParameterValues() {
        return parameterValues;
    }

    public String[] getParameterNames() {
        return parameterNames;
    }

    public String getClassSimpleName() {
        return targetClass.getSimpleName();
    }

    public String getMethodName() {
        return targetMethod.getName();
    }



}
