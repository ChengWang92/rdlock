# rdLock使用

### 介绍
rdLock分布式锁（支持jedis/redisson/zk），可根据项目实际使用哪款中间件来配置，支持注解、手动加解锁，简单方便

支持jedis/redisson/curator



### 快速开始

pom

```properties
#后续升级功能建议使用版本号方式
<dependency>
  <groupId>com.bsd.xxyp</groupId>
  <artifactId>rdlock</artifactId>
  <version>0.0.1-SNAPSHOT</version>
</dependency>

```

考虑到项目相关配置差异化，将该配置放入业务侧自定义配置

**注意：使用rdLock前提项目中必须有该如下配置类中的一个**

`JedisCluster/RedissonClient/CuratorFramework`

可拔插配置`bsd.xxxp.lock.enable`

不设置值默认开启rdLock功能

可在配置文件中设置`bsd.xxxp.lock.enable=false`，不启用该功能




### 注解说明

`@RdLock` 核心锁注解

| **字段** | **类型** | **说明** |
| --- | --- | --- |
| bizKey | string | 业务key前缀,如果bizKey不传默认类名+方法名,如果bizKey和动态参数value都不传,这样会只允许一个线程进入(根据业务需求设定合理的key) |
| value | string | 动态参数值 el表达式 格式单个 #xxx,多个用{#xxx,#xxx},对象使用 #user.getName()或者#user.name，也可使用@LockKey注解来标记 |
| allowValueBlank | boolean | 是否允许动态参数value有空值，不允许默认抛异常提示 |
| lockTimeOut | long | 锁超时释放时间(单位：毫秒),避免死锁(根据业务合理设定) |
| attemptedWait | boolean | 当获取锁失败时,是否尝试阻塞重新获取锁 |
| retryCount | int | 获取锁失败时，重试次数 默认:3 |
| retryInterval | long | 当获取锁失败时,重试等待间隔 默认：500L(单位:毫秒) |
| errMsg | string | 当获取锁失败时,异常提示 配合项目中统一异常处理 |
| executor | LockExecutor.Executor | 自定义选择执行器,此次操作使用的执行器 |
| autoRelease | boolean | 执行完是否释放锁，否则等待锁过期时间，不支持zk使用 |

value动态参数属性解析如果不使用el解析，也可以使用以下注解来代替


`@LockKey` 标记参数字段，拼接该参数值作为锁key

- 该注解属性`sort`字段，用于标记多个key时指定拼接顺序,默认字段从左到右拼接顺序,指定了sort属性可以指定拼接顺序

`@LockBean`  当需要获取参数bean里的某个字段作为锁key时，标记该参数是个bean，需要配合`@LockKey`使用

解析该bean里的`@LockKey`标记的字段值

### 全局配置说明

```properties
#全局默认执行器,不设置值默认取可用的一个
bsd.xxxp.lock.executor-name=jedis/redisson/zk
#全局key前缀,不设置值默认取项目服务名
bsd.xxxp.lock.prefix=xx
```

### key组装说明

默认规则： `prefix:bizKey:value`

- `prefix`:全局key前缀，如果不指定，默认会取项目中spring.application.name配置的值
- `bizKey` : 业务key前缀
- `value`: el表达式
- `@LockKey` :标记参数字段作为锁值

根据`@RdLock`属性`value`是否为空来选择不同的解析方式

- value属性`为空`解析`@LockKey`注解标记的字段

- value属性`有值`解析el表达式

**注：动态参数值不要有null值或空值作为锁key，否则默认会报错提示 ,可用属性valueBlankThrowE = false 不抛异常**

rdLock提供了可自定义拼接key的扩展接口

继承`DefaultLockKeyProcessor`类进行自定义key的拼接操作

```java
@Component
public class CustomKeyProcessor extends DefaultLockKeyProcessor {

    public CustomKeyProcessor(LockHolder lockHolder) {
        super(lockHolder);
    }

    @Override
    public String buildLockKey(LockAnnotationDataExtractor extractor) {
        System.out.println("我是自定义key啊");
        return super.buildLockKey(extractor);
    }
}
```

### 执行器使用说明

全局执行器配置

在`application.properties或者yml文件`中配置 `bsd.xxxp.lock.executor-name=redisson`

可选的全局默认执行器: `jedis/redisson/zk`,不设置值默认取一个可用的执行器

当项目中配置如下配置类才会加载对应执行器

`JedisCluster->jedis`

`RedissonClient->redisson`

`CuratorFramework->zk`

自定义执行器

`RdLock`属性`executor`选择本次执行的执行器，如果该所选执行器没有配置则取默认执行器

```java
@RdLock(bizKey = "create:order",executor = LockExecutor.Executor.ZK)
public void test4(@LockBean Student student) {

}
```

### 使用示例

锁key: create:order:userId参数值:student对象里score参数值

```java
@RdLock(bizKey = "create:order",value = "{#userId,#student.score}")
public void test3(Integer userId, String name, Student student) {

}
```

锁key: 类名:test2(方法名):userId参数值

```
@RdLock
public void test2(@LockKey Integer userId, String name) {

}
```

锁key: create:order:name参数值:userId参数值

```java
@RdLock(bizKey = "create:order")
public void test4(@LockKey(sort = 2) Integer userId,@LockKey(sort = 1) String name) {

}
```

锁key: create:order:student对象里参数id值，执行器使用zk

```java
@RdLock(bizKey = "create:order",executor = LockExecutor.Executor.ZK)
public void test4(@LockBean Student student) {

}

@Data
public class Student {

    @LockKey
    private Integer id;
    
    private String score;
}
```

### lockTemplate 手动加锁使用
**注意：如果关闭rdLock功能 ，那么该手动加解锁将失效，为了不影响业务正常执行，加解锁默认返回true**

按照如下格式执行加锁解锁

```java
@Autowired
private LockTemplate lockTemplate;

try{
    boolean lock = lockTemplate.lock("create:order:service:1");
    
    //业务逻辑
}finally {
    lockTemplate.unLock();
}
```

示例：

```java
    try {
            boolean lock = lockTemplate.lock("create:order:service:1");
            if (!lock) {
                System.out.println("没加锁成功处理。。。。");
                throw new RdLockException("请稍后再试");
            }
            //加锁成功业务处理

            System.out.println("业务加锁成功啦");
            //boolean lock1 = lockTemplate.lock("buy:benefit:goods:123");
        } finally {
            boolean b = lockTemplate.unLock();
            System.out.println("业务释放锁:" + b);
        }
```

重入加锁示例：

**注意：有几个加锁需对应几个解锁,且加锁如果选定了执行器，那么解锁也需要对应选上执行器**

```java
	try {
            boolean lock1 = lockTemplate.lock("create:order:service:2");
            if (lock1) {
                //加锁成功处理逻辑
                System.out.println("业务1加锁成功啦:"+lock1);

                try {
                    //第二把锁，选择自定义执行器
                    boolean lock2 = lockTemplate.lock("buy:benefit:goods:123", LockExecutor.Executor.ZK);
                    System.out.println("业务2加锁成功啦:"+lock2);
                } finally {
                    boolean b = lockTemplate.unLock(LockExecutor.Executor.ZK);
                    System.out.println("业务2释放锁:" + b);
                }

            } else {
                System.out.println("加锁失败");
            }
        } finally {
            boolean b = lockTemplate.unLock();
            System.out.println("业务1释放锁:" + b);
        }
```

### 记录锁操作日志
当需要记录本次执行锁的一些信息，可以实现`LockResultHandler`接口，自定义记录
```java
@Component
    public class CustomLockResultHandler implements LockResultHandler {
        @Override
        public void record(ResultRecord resultRecord) {
            System.out.println(JSON.toJSONString(resultRecord));
        }
    }
```




